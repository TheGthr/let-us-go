/*INSERT INTO public.snippets (title, content, created, expires) VALUES (
                                                                          'An old silent pond',
                                                                          'An old silent pond...\nA frog jumps into the pond,\nsplash! Silence again.\n\n– Matsuo Bashō',
                                                                          NOW(),
                                                                          DATE_ADD(NOW(), INTERVAL '365 day')
                                                                      );
INSERT INTO public.snippets (title, content, created, expires) VALUES (
                                                                          'Over the wintry forest',
                                                                          'Over the wintry\nforest, winds howl in rage\nwith no leaves to blow.\n\n– Natsume Soseki',
                                                                          NOW(),
                                                                          DATE_ADD(NOW(), INTERVAL '365 day')
                                                                      );
INSERT INTO public.snippets (title, content, created, expires) VALUES (
                                                                          'First autumn morning',
                                                                          'First autumn morning\nthe mirror I stare into\nshows my father''s face.\n\n– Murakami Kijo',
                                                                          NOW(),
                                                                          DATE_ADD(NOW(), INTERVAL '7 day')
                                                                      );*/

CREATE USER web WITH password 'pass';
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO web;
-- Important: Make sure to swap 'pass' with a password of your own choosing.
