package main

import "net/http"

func (app *application) routes() *http.ServeMux {
	fileServer := http.FileServer(http.Dir("./ui/static/"))

	// Use the http.NewServeMux() function to initialize a new servemux, then
	// register the home function as the handler for the "/" URL pattern.
	// Create this new ServeMux instead of using globally defined DefaultServeMux for security reasons
	mux := http.NewServeMux()

	mux.HandleFunc("/", app.home)
	mux.HandleFunc("/snippet/view", app.snippetView)
	mux.HandleFunc("/snippet/create", app.snippetCreate)
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))

	return mux
}
